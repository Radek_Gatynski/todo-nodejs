var bodyPaser = require('body-parser');
var mongoose = require('mongoose');
//connect 
mongoose.connect('mongodb+srv://radgat:Galaxy9500@cluster0-j4toj.mongodb.net/test?retryWrites=true');
let urlencodedParser = bodyPaser.urlencoded({extended: false});
var todoSchema = new mongoose.Schema({
    item: String
});
var Todo = mongoose.model('Todo', todoSchema);

module.exports = function(app){
        app.get('/todo', function(req, res){
            Todo.find({}, function(err, data){
                if(err) throw err;
                res.render('todo', {todos: data});
            })
            
        });
        app.post('/todo', urlencodedParser, function(req, res){
            var newTodo = Todo(req.body).save(function(err, data){
                if(err) throw err;
                res.json(data);
            })
        });
        app.delete('/todo/:item', function(req, res){
            Todo.find({item: req.params.item.replace(/\-/g, " ")}).remove(function(err, data){
                if(err) throw err;
                res.json(data);
            })
        });
};

